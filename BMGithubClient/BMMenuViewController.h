//
//  BMMenuViewController.h
//  BMGithubClient
//
//  Created by Vinzenz-Emanuel Weber on 07.01.15.
//  Copyright (c) 2015 blockhaus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BMMenuViewController : UITableViewController

@end
