//
//  BMUserViewController.m
//  BMGithubClient
//
//  Created by Vinzenz-Emanuel Weber on 07.01.15.
//  Copyright (c) 2015 blockhaus. All rights reserved.
//

#import "BMUserViewController.h"
#import "AppDelegate.h"
#import <AFNetworking/UIImageView+AFNetworking.h>


@interface BMUserViewController ()

@end

@implementation BMUserViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:@"https://api.github.com/user"
      parameters:@{@"access_token": delegate.accessToken}
         success:^(AFHTTPRequestOperation *operation, id responseObject) {

             NSLog(@"JSON: %@", responseObject);
             
             // set name of user
             self.usernameLabel.text = [responseObject valueForKey:@"name"];
             
             // and load the user avatar image
             NSString *avatarURLString = [responseObject valueForKey:@"avatar_url"];
             NSURL *avatarURL = [NSURL URLWithString:avatarURLString];
             if (avatarURL) {
                 [self.userProfileImageView setImageWithURL:avatarURL];
             }
             
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
