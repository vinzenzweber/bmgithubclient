//
//  BMUserViewController.h
//  BMGithubClient
//
//  Created by Vinzenz-Emanuel Weber on 07.01.15.
//  Copyright (c) 2015 blockhaus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BMUserViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *userProfileImageView;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;

@end
