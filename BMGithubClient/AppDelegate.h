//
//  AppDelegate.h
//  BMGithubClient
//
//  Created by Vinzenz-Emanuel Weber on 07.01.15.
//  Copyright (c) 2015 blockhaus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <AFNetworking/AFNetworking.h>


#define kCLIENT_ID @"4ac9d95adb52629e22fa"
#define kCLIENT_SECRET @"e7e9393d8db2596fdd30d56735ffd6fe8930936b"


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;


@property (strong, nonatomic) NSString *accessToken;
- (void)authorize;


@end

