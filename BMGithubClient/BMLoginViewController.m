//
//  BMLoginViewController.m
//  BMGithubClient
//
//  Created by Vinzenz-Emanuel Weber on 07.01.15.
//  Copyright (c) 2015 blockhaus. All rights reserved.
//

#import "BMLoginViewController.h"
#import "AppDelegate.h"


@interface BMLoginViewController ()

@end

@implementation BMLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)authorize:(id)sender {
   
    NSString *authorizeURLString = [NSString stringWithFormat:@"https://github.com/login/oauth/authorize?scope=user:email&client_id=%@", kCLIENT_ID];
    NSURL *authorizeURL = [NSURL URLWithString:authorizeURLString];
    [[UIApplication sharedApplication] openURL:authorizeURL];

}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
